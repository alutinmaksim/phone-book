<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Contact>
 */
class ContactFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $user = User::all()->random();
        $phone = fake()->phoneNumber;
        return [
            'name' => fake()->name,
            'surname' => fake()->lastName,
            'user_id' => $user->id,
            'identifier' => $phone . $user->id,
            'phone' => $phone
        ];
    }
}
