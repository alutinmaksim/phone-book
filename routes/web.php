<?php

use App\Http\Controllers\CSV\ExportController;
use App\Http\Controllers\CSV\ImportController;
use App\Http\Controllers\Main\ContactController;
use App\Http\Controllers\Main\IndexController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [IndexController::class, 'index'])->middleware('guest');
Route::group(['middleware' => 'auth'], function () {
    Route::get('/search', [IndexController::class, 'search'])->name('search');
    Route::resource('contacts', ContactController::class)->except('show');
    Route::post('/import', [ImportController::class, 'import'])->name('import');
    Route::get('/export', [ExportController::class, 'export'])->name('export');
});

Auth::routes();

Route::fallback(function () {
    return redirect()->to('/contacts');
});

