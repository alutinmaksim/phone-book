<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Http\Requests\Contact\SearchRequest;
use App\Models\Contact;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function index(): View
    {
        return view('auth.login');
    }

    public function search(SearchRequest $request): View
    {
        $str = '%' . $request->search . '%';
        $contacts = Contact::query()
            ->where('user_id', auth()->id())
            ->where(function (Builder $query) use ($str) {
                $query->where('name', 'LIKE', $str)
                    ->orWhere('surname', 'LIKE', $str)
                    ->orWhere('phone', 'LIKE', $str);
            })->paginate(3);

        $contacts->appends(['search' => $str]);
        return view('contacts.index', compact('contacts'));
    }
}
