<?php

namespace App\Http\Controllers\CSV;

use App\Http\Controllers\Controller;
use App\Imports\ContactImport;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller
{
    public function import(): RedirectResponse
    {
        Excel::import(new ContactImport(), request()->file('file'));
        return redirect()->route('contacts.index');
    }
}
