<?php

namespace App\Http\Controllers\CSV;

use App\Exports\ContactExport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ExportController extends Controller
{
    public function export(): BinaryFileResponse
    {
        return Excel::download(new ContactExport, 'contact.csv');
    }
}
