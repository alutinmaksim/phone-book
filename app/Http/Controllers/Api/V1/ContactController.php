<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Contact\StoreRequest;
use App\Http\Requests\Api\Contact\UpdateRequest;
use App\Http\Resources\Contact\ContactResource;
use App\Models\Contact;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        $id = $request->user()->id;
        $contacts = Contact::query()->where('user_id', $id)->get();

        return ContactResource::collection($contacts);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request): ContactResource
    {
        $data = $request->validated();
        $data['phone'] = (string) $data['phone'];
        $contact = Contact::query()->create($data);

        return new ContactResource($contact);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Contact $contact): ContactResource
    {
        return new ContactResource($contact);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Contact $contact): ContactResource
    {
        $data = $request->validated();
        $data['phone'] = (string) $data['phone'];
        $contact->update($data);

        return new ContactResource($contact);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Contact $contact): JsonResponse
    {
        $contact->delete();

        return response()->json([
            'status' => true,
            'message' => 'Contact deleted',
        ]);
    }
}
