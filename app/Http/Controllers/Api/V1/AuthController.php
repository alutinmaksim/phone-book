<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(LoginRequest $request): JsonResponse|UserResource
    {
        $data = $request->validated();
        if (!Auth::attempt($data)) {
            return response()->json([
                'status' => false,
                'message' => 'Email & Password does not match with our record.',
            ], 401);
        }
        $user = User::query()->where('email', $data['email'])->first();
        $user['api_token'] = $user->createToken("api_token")->plainTextToken;

        return new UserResource($user);
    }

    public function register(RegisterRequest $request): UserResource
    {
        $data = $request->validated();
        $user = User::query()->create($data);
        $user['api_token'] = $user->createToken("api_token")->plainTextToken;

        return new UserResource($user);
    }

    public function logout(Request $request): JsonResponse
    {
        $request->user()->currentAccessToken()->delete();

        return response()->json([
            'status' => true,
            'message' => 'Logout saccess',
        ], 200);
    }
}
