<?php

namespace App\Http\Requests\Api\Contact;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['nullable', 'string'],
            'surname' => ['nullable', 'string'],
            'phone' => ['required', 'integer', 'min:5'],
            'user_id' => ['required', 'integer'],
            'identifier' => ['required', 'string', 'unique:contacts'],
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge([
            'user_id' => (string) $this->user()->id,
            'identifier' => $this->phone . $this->user()->id
        ]);
    }
}
