<?php

namespace App\Imports;

use App\Models\Contact;
use Maatwebsite\Excel\Concerns\ToModel;

class ContactImport implements ToModel
{
    public function startRow(): int
    {
        return 2;
    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model
    */
    public function model(array $row): Contact
    {
        return new Contact([
            'user_id'    => auth()->id(),
            'name'       => $row[0],
            'surname'    => $row[1],
            'phone'      => $row[2],
            'identifier' => $row[2] . auth()->id()
        ]);
    }
}
