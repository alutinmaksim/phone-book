<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Contact extends Model
{
    use HasFactory;

    protected $table = 'contacts';
    protected $fillable = [
        'user_id',
        'name',
        'surname',
        'phone',
        'identifier'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
