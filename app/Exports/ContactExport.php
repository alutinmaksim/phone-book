<?php

namespace App\Exports;

use App\Models\Contact;
use Maatwebsite\Excel\Concerns\FromCollection;

class ContactExport implements FromCollection
{
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ';'
        ];
    }

    public function headings(): array
    {
        return ["Name", "Surname", "Phone"];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Contact::query()
            ->where('user_id', auth()->id())
            ->select('name', 'surname', 'phone')
            ->get();
    }
}
