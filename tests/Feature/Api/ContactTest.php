<?php

namespace Tests\Feature\Api;

use App\Models\Contact;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ContactTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->withHeaders([
            'Accept' => 'application/json'
        ]);
    }

    /** @test */
    public function contact_can_be_stored()
    {
        $this->withoutExceptionHandling();

        $data = [
            'name' => 'Name',
            'surname' => 'Surname',
            'phone' => '5550807'
        ];

        $user = User::factory()->create();
        $response = $this->actingAs($user)
            ->post('api/V1/contacts', $data);
        $this->assertDatabaseCount('contacts', 1);
        $contact = Contact::query()->first();
        $this->assertEquals($data['name'], $contact->name);
        $this->assertEquals($data['surname'], $contact->surname);
        $this->assertEquals($data['phone'], $contact->phone);
        $response->assertJson([
            'data' => [
                'id' => $contact->id,
                'name' => $contact->name,
                'surname' => $contact->surname,
                'phone' => $contact->phone,
            ]
        ]);
    }

    /** @test */
    public function field_phone_is_required_for_store_contact()
    {
        $data = [
            'name' => 'Name',
            'surname' => 'Surname',
            'phone' => ''
        ];

        $user = User::factory()->create();
        $response = $this->actingAs($user)
            ->post('api/V1/contacts', $data);
        $response->assertStatus(422);
        $response->assertInvalid('phone');
    }

    /** @test */
    public function contact_can_be_updated()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->hasContacts(1)->create();
        $contact = Contact::query()->first();
        $data = [
            'name' => 'Name edited',
            'surname' => 'Surname edited',
            'phone' => '5550807'
        ];

        $response = $this->actingAs($user)
            ->patch('api/V1/contacts/' . $contact->id, $data);

        $updatedContact = Contact::query()->first();
        $this->assertEquals($data['name'], $updatedContact->name);
        $this->assertEquals($data['surname'], $updatedContact->surname);
        $this->assertEquals($data['phone'], $updatedContact->phone);
        $this->assertEquals($contact->id, $updatedContact->id);
        $response->assertJson([
            'data' => [
                'id' => $updatedContact->id,
                'name' => $updatedContact->name,
                'surname' => $updatedContact->surname,
                'phone' => $updatedContact->phone,
            ]
        ]);
    }

    /** @test */
    public function contact_can_be_deleted()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->hasContacts(1)->create();
        $contact = Contact::query()->first();

        $response = $this->actingAs($user)
            ->delete('api/V1/contacts/' . $contact->id);
        $this->assertDatabaseCount('contacts', 0);
        $response->assertJson([
            'status' => true,
            'message' => 'Contact deleted',
        ]);
    }

    /** @test */
    public function response_for_route_contacts_with_all_contacts()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->hasContacts(10)->create();
        $contacts = Contact::all();
        $contacts = $contacts->map(function ($contact) {
            return [
                'id' => $contact->id,
                'name' => $contact->name,
                'surname' => $contact->surname,
                'phone' => $contact->phone,
            ];
        })->toArray();
        $json = [
            'data' => $contacts
        ];
        $response = $this->actingAs($user)
            ->get('api/V1/contacts');
        $response->assertOk();
        $response->assertJson($json);
    }

}
