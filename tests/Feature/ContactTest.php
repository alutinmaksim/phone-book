<?php

namespace Tests\Feature;

use App\Models\Contact;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ContactTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function contact_can_be_stored()
    {
        $this->withoutExceptionHandling();

        $data = [
            'name' => 'Name',
            'surname' => 'Surname',
            'phone' => '5550807'
        ];

        $user = User::factory()->create();
        $response = $this->actingAs($user)
            ->post('/contacts', $data);
        $response->assertRedirect('/contacts');
        $this->assertDatabaseCount('contacts', 1);
        $contact = Contact::query()->first();
        $this->assertEquals($data['name'], $contact->name);
        $this->assertEquals($data['surname'], $contact->surname);
        $this->assertEquals($data['phone'], $contact->phone);
    }

    /** @test */
    public function contact_can_be_updated()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->hasContacts(1)->create();
        $contact = Contact::query()->first();
        $data = [
            'name' => 'Name edited',
            'surname' => 'Surname edited',
            'phone' => '5550807'
        ];

        $response = $this->actingAs($user)
            ->patch('/contacts/' . $contact->id, $data);
        $response->assertRedirect('/contacts');

        $updatedContact = Contact::query()->first();
        $this->assertEquals($data['name'], $updatedContact->name);
        $this->assertEquals($data['surname'], $updatedContact->surname);
        $this->assertEquals($data['phone'], $updatedContact->phone);
        $this->assertEquals($contact->id, $updatedContact->id);
    }

    /** @test */
    public function contact_can_be_deleted()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->hasContacts(1)->create();
        $contact = Contact::query()->first();

        $response = $this->actingAs($user)
            ->delete('/contacts/' . $contact->id);
        $response->assertRedirect('/contacts');
        $this->assertDatabaseCount('contacts', 0);
    }

    /** @test */
    public function response_for_method_index_should_be_view_with_contacts()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->hasContacts(10)->create();
        $contact = Contact::query()->first();

        $response = $this->actingAs($user)
            ->get('/contacts');
        $response->assertViewIs('contacts.index');
    }

    /** @test */
    public function response_for_method_edit_should_be_view_with_contact()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()->hasContacts(1)->create();
        $contact = Contact::query()->first();

        $response = $this->actingAs($user)
            ->get("/contacts/{$contact->id}/edit");
        $response->assertViewIs('contacts.edit');
    }

}
