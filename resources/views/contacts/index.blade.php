@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form action="{{ route('search') }}" method="GET" class="mb-2">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Поиск"
                               aria-label="Recipient's username" aria-describedby="button-addon2" name="search">
                        <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Найти</button>
                    </div>
                    @error('search')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </form>
                <div class="card">
                    <div class="card-header">{{ __('Добавить запись') }}</div>

                    <div class="card-body">
                        <form action="{{ route('contacts.store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <input name="name" placeholder="Имя" class="form-control mb-2">
                                @error('name')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input name="surname" placeholder="Фамилия" class="form-control mb-2">
                                @error('surname')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input name="phone" placeholder="Телефон" class="form-control mb-2">
                                @error('phone')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                                @error('identifier')
                                <div class="text-danger">This phone number already exists</div>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary float-right">Добавить</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 my-2">
                <hr>
            </div>
        </div>
    </div>

    @if($contacts->count() > 0)
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    @foreach($contacts as $contact)
                        <div class="card mb-2">

                            <div class="card-header d-flex justify-content-between">
                                <div>
                                    {{ $contact->name }} {{ $contact->surname }}
                                </div>
                                <div>
                                    <a href="{{ route('contacts.edit', $contact->id) }}"
                                       class="link-underline link-underline-opacity-0">&#9998;</a>
                                </div>
                            </div>
                            <div class="card-body">
                                {{ $contact->phone }}
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @else
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <p>У Вас нет записей.</p>
                </div>
            </div>
        </div>
    @endif

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 my-2">
                {{ $contacts->links() }}
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Загрузить данные</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="import" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="input-group mb-3">
                            <input type="file" name="file" class="form-control">
                            <button class="btn btn-primary" type="submit">Загрузить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection

