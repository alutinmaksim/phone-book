@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Изменить запись') }}</div>

                    <div class="card-body">
                        <form action="{{ route('contacts.update', $contact->id) }}" method="POST">
                            @csrf
                            @method('PATCH')
                            <div class="form-group">
                                <input name="name" placeholder="Имя" class="form-control mb-2" value={{ $contact->name }}>
                                @error('name')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input name="surname" placeholder="Фамилия" class="form-control mb-2" value={{ $contact->surname }}>
                                @error('surname')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input name="phone" placeholder="Телефон" class="form-control mb-2" value={{ $contact->phone }}>
                                @error('phone')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary">Изменить</button>
                        </form>
                        <form action="{{ route('contacts.destroy', $contact->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger mt-2">Удалить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

